package com.company;

import java.util.Random;

class Computer {

    public GameOptions getAction() {
        GameOptions[] actions = GameOptions.values();
        Random random = new Random();
        return actions[random.nextInt(actions.length)];
    }
}
