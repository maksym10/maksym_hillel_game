package com.company;

enum GameOptions {
    rock, paper, clippers;

    public int compareMoves(GameOptions otherMove) {

        if (this == otherMove)
            return 0;

        switch (this) {
            case rock:
                return (otherMove == clippers ? 1 : -1);
            case paper:
                return (otherMove == rock ? 1 : -1);
            case clippers:
                return (otherMove == paper ? 1 : -1);
        }

        return 0;
    }
}

public class GameLogic {
    private final User user;
    private final Computer computer;
    private int userScore;
    private int computerScore;
    private int numberOfGames;

    public GameLogic() {
        user = new User();
        computer = new Computer();
        userScore = 0;
        computerScore = 0;
        numberOfGames = 0;
    }


    public void startGame() {
        System.out.println("Game starts!");
        GameOptions userMove = user.getAction();
        GameOptions computerMove = computer.getAction();
        System.out.println("\nYour turn  " + userMove + ".");
        System.out.println("Computer turn  " + computerMove + ".\n");

        int compareActions = userMove.compareMoves(computerMove);
        switch (compareActions) {
            case 0:
                System.out.println("Draw!");
                break;
            case 1:
                System.out.println(userMove + " beats " + computerMove + ". You're won!");
                userScore++;
                break;
            case -1:
                System.out.println(computerMove + " beats " + userMove + ". You're lose.");
                computerScore++;
                break;
        }
        numberOfGames++;
        printGameStats();

        if (User.tryOneMoreTime()) {

            System.out.println();
            startGame();
        } else {
            printGameStats();
        }
    }

    private void printGameStats() {
        int wins = userScore;
        int losses = computerScore;
        int ties = numberOfGames - userScore - computerScore;
        double percentageWon = (wins + ((double) ties) / 2) / numberOfGames;


        System.out.printf("\n|  %6s  |  %6s  |  %6s  |  %12s  |  %14s  |\n",
                "Winnings", "Losses", "Draws", "Total games", "Winning percentrage");


        System.out.printf("|  %6d    |  %6d  |  %6d  |  %12d  |  %13.2f%%       |\n\n",
                wins, losses, ties, numberOfGames, percentageWon * 100);

    }
}
