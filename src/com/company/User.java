package com.company;

import java.util.Scanner;


class User {

    private static Scanner inputUser;

    public User() {
        inputUser = new Scanner(System.in);
    }

    public GameOptions getAction() {
        System.out.println("Pls choose -rock -paper -clippers");

        String inputs = inputUser.nextLine();
        inputs = inputs.toLowerCase();
        if (inputs.equals("rock") || inputs.equals("paper") || inputs.equals("clippers")) {
            switch (inputs) {
                case "rock":
                    return GameOptions.rock;
                case "paper":
                    return GameOptions.paper;
                case "clippers":
                    return GameOptions.clippers;
            }
        }
        System.out.println("Incorrect variant. Pls try to write your variant in lower case");
        return getAction();
    }

    public static boolean tryOneMoreTime() {
        System.out.println("One more time? \nIf yes write 'y'\nIf no write 'n'");

        String userInput;
        char answerLetter;

        userInput = inputUser.nextLine().toLowerCase();

        if (userInput.equals("y") || userInput.equals("n")) {
            answerLetter = userInput.charAt(0);
            switch (answerLetter) {
                case 'y':
                    return true;
                case 'n':
                    System.out.println("Game over");
                    System.exit(0);
            }
        } else {
            System.out.println("Incorrect value.\n");
            return tryOneMoreTime();
        }


        return false;
    }
}
